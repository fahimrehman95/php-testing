<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress2' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_J$$;VySGXK^BzA27u:~#uTeu$//h#|Y-DjQV!nm[Rm+lT=%5C8+^0~g(Xh0Ik@r');
define('SECURE_AUTH_KEY',  '5,n7D*jbrL4a RyM0><xJ}xuDr?CQ858&+J[Hvo[)qY79j B ^a-UQ&[1 1R?N.u');
define('LOGGED_IN_KEY',    '+R[0-Yu+?S*-,`.4T.rA?)O+uz#SXZQL!}TMsTpHR+37OP>7%P-Y&WxSw~)_r`t:');
define('NONCE_KEY',        '7t,9$)Gj-i!h^3 .l:MU7s`#+zodb!!YZ1BP-BVskxGG;g1.:(?32~NjWl,<`A~-');
define('AUTH_SALT',        'a:`{|Q6Y1iwH.^q{ 5c.YR[bNey8&yrNN+-M9V zF;-El*e?|70ItP]Z>ZS9cbt[');
define('SECURE_AUTH_SALT', 'xZ.zD--Y2k?~:EY8H-hUt`@0L;ki}1KSz2C@iA&ni=H;<@F;L{Q+vg*6)Ta3Y9IP');
define('LOGGED_IN_SALT',   '|bygXe42yf~q<!c#-HF>)6!|uWW r~|>4>jti3i2/2)vp|?ViJ-yEP&~O_b~T)+x');
define('NONCE_SALT',       'd6{O$?Mz/ef/KKaA-XJ5i[xrw)R5q>&t`S^GVk/,isb>ZF>W9i2z@|)0DSc2oz2W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
